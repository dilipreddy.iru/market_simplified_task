
import styled from 'styled-components/native';
import {  Text, ListItem, Body, Button } from 'native-base';

export const ProductListItem = styled(ListItem)`
  border-color: gray;
  padding: 10px 5px;
`;
export const WhiteBoldText = styled(Text)`
  color: white;
  font-size: 16px;
  width: 150px;
  text-align: right;
`;

export const ActionButtons = styled(Button)`
margin: 10px;
 width: 50px;
 text-align: center;
 align-items: center;
 justify-content: center;
 background-color: #FF6A3D;
`;
export const WhiteText = styled(Text)`
  color: white;
  font-size: 12px;
`;

export const ImageTitleText = styled(Text)`
  color: white;
  font-style: italic;
  font-size: 12px;
`;
export const WhiteBoldTitleText = styled(Text)`
  color: white;
  font-size: 14px;
`;


export const IconWrapperViewSub = styled.View`
  flex-direction: row;
`;

export const IconWrapperView = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-around;
`;


export const BodyView = styled(Body)`
flex: 0.8;
margin-left: ${props => !props.padLeft ? '55px': 0};
`;