/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Thumbnail, Icon, Right, Button } from 'native-base';
import {
  WhiteText,
  ProductListItem,
  ImageTitleText,
  WhiteBoldTitleText,
  IconWrapperView,
  IconWrapperViewSub,
  BodyView,
  ActionButtons
} from './styled';

class ShopList extends Component {
  constructor(props) {
    super(props);
  }

  
  render() {
    const {
      imageUri,
      name,
      description,
      imgTitle,
      onSub,
      style,
    } = this.props;
    const showImage = imageUri && imageUri.uri !== null;
    return (
      <ProductListItem
        noIndent
        onPress={onSub}
        underlayColor="transparent"
        style={style}>

        {showImage && <Thumbnail circle source={{ uri: imageUri.uri }} />}
        <BodyView padLeft={showImage}>
          <WhiteBoldTitleText variant="default">{name}</WhiteBoldTitleText>
          <WhiteText variant="default" note numberOfLines={1}>
            {description}
          </WhiteText>
        </BodyView>

        <Right>
          <IconWrapperView>
            <IconWrapperViewSub>
              <ActionButtons rounded onPress={onSub}>
                <Icon style={{ fontSize: 18 }} name="delete" type="MaterialCommunityIcons" />
              </ActionButtons>
            </IconWrapperViewSub>
          </IconWrapperView>
          <ImageTitleText variant="primary">{imgTitle} </ImageTitleText>
        </Right>
      </ProductListItem>
    );
  }
}
export default ShopList;
