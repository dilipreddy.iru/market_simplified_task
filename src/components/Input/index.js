import React, { Component } from 'react';
import {IconHead, ItemView } from './styled';
import {  Input} from 'native-base';


class DarkInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
    };
  }
  render() {
    let {
      icon,
      onClick,
      style,
      type,
      value,
      onChangeText,
      placeholder,
      placeholderTextColor,
      keyboardType,
      focus
    } = this.props;
    const showIcon = icon && icon.length > 0;
    if (!style) {
      style = { color: 'white'};
    }
    return (
      <ItemView
            style={style}
       >
        {showIcon === true && (
          <IconHead
            active
            type={type}
            name={icon}
            onPress={onClick}
            style={style}
          />
        )}
        <Input
          placeholder={placeholder}
          onFocus={focus}
          keyboardType={keyboardType}
          placeholderTextColor={placeholderTextColor}
          value={value}
          onChangeText={val => {
            onChangeText(val);
          }}
          style={style}
        />
      </ItemView>
    );
  }
}

export default DarkInput;
