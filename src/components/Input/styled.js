import styled from 'styled-components/native';
import { Icon, Item, Input, Label, Text } from 'native-base';



export const IconHead = styled(Icon)`
  color: white;
  margin-left: 3%;
`;

export const WhiteLabel = styled(Label)`
  color: white;
`;
export const WhiteText = styled(Label)`
  color: white;
  font-size: 12px;
  margin-top: 5px;
`;
export const ItemView = styled(Item)`
  border-bottom-width: 1px;

  border-top-width: 1px;
  border-left-width: 1px;
  border-right-width: 1px;
  border-color: white;
  border-radius: 10px;
  margin-bottom: 10px;
`;

export const InputView = styled(Input)`
  font-size: 12px;
`;

export const BorderBottomView = styled(Input)`
  color: white;
  font-size: 12px;
`;
