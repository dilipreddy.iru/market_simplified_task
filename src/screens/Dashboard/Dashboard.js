import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, View, Card, Button, Icon } from 'native-base';
import { BackHandler } from 'react-native'
import Modal from 'react-native-modal';
import axios from 'axios'
import { MainContentDashboard } from '../styled'
import ProductList from '../../components/List'

export default class ListAvatarExample extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false,
            products: []
        };
        this.onDelete = this.onDelete.bind(this)

    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    onYesClick = () => {
        this.props.navigation.push('Home');
        this.setState({
            isModalVisible: false
        })
    }
    onButtonPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        // then navigate
        navigate('Login');
    }
    handleBackButton = () => {

        this.setState({
            isModalVisible: true
        })
        return true;
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        axios.get("http://hplussport.com/api/products/").then((response) => {
            console.log("response", response.data);
            response.data.sort(function(a, b){
                if(a.name < b.name) { return -1; }
                if(a.name > b.name) { return 1; }
                return 0;
            }) 
            this.setState({
                products: response.data
            })
        })
    }

    onDelete = (id) => {
        const updatedProducts = this.state.products.filter(del => del.id !== id);
        this.setState({products: updatedProducts})
       
        // console.log('id', id);
      }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    render() {


            const {products} = this.state;

        return (
            <Container>
                <Header style={{ alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontSize: 20 }}>Dashboard</Text>
                </Header>
                <Content style={{backgroundColor: '#0b6ea5'}}>
                    {
                        products && products.map((item) => {
                            return (
                                <ProductList
                                    id={item.id}
                                    key={item.id}
                                    imageUri={{ uri: item.image }}
                                    name={item.name}
                                    description={item.description}
                                    onSub={() => this.onDelete(item.id)}
                                />
                            )
                        })
                    }


                    <View>
                        <Modal isVisible={this.state.isModalVisible}>
                            <View style={{ flex: 1 }}>
                                <MainContentDashboard style={{ padding: 10, margin: 10 }}>

                                    <Card style={{ padding: 10, margin: 10 }}>

                                        <View>
                                            <Text>Are You Sure you want to Logout ?</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{ width: '50%', alignItems: 'center' }}>
                                                <Button transparent onPress={this.onYesClick} style={{ margin: 10 }}>
                                                    <Text>Yes</Text>
                                                </Button>
                                            </View>
                                            <View style={{ width: '50%', alignItems: 'center' }}>
                                                <Button transparent onPress={this.toggleModal} style={{ margin: 10 }}>
                                                    <Text>No</Text>
                                                </Button>
                                            </View>
                                        </View>
                                    </Card>

                                </MainContentDashboard>

                            </View>
                        </Modal>
                    </View>
                </Content>
            </Container>
        );
    }
}