import styled from 'styled-components/native';
import { Icon, Item, Input, Label, Text } from 'native-base';


export const MainContent = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-content: center;
  padding-right: 30px;
  padding-left: 30px;
  background-color: #0b6ea5
`;

export const MainContentDashboard = styled(MainContent)`
  background-color: transparent
`;
