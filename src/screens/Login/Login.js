import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet } from "react-native";
import axios from "axios";
import { Container, Header, Content, Item, Input, Button, Text, View } from 'native-base';

import DarkInput from '../../components/Input'
import { MainContent } from '../styled'
import { BarPasswordStrengthDisplay } from 'react-native-password-strength-meter';


const userDetails = { email: 'test@gmail.com', password: 'test' }
export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = { email: '', password: '', showPwdMeter: false };
    }
    onEmailChange(value) {
        this.setState({
            email: value
        });
    }
    onPasswordChange(value) {
        this.setState({
            password: value
        });
    }

    
    onPress = () => {
        if (userDetails.email === this.state.email && userDetails.password === this.state.password) {
            this.props.navigation.push('Dashboard');
            this.setState({
                email: '',
                password: ''
            })
        }
        else if (this.state.email === '' || this.state.password === '') {
            alert('please enter email or password')
        }
        else {
            alert('username or password incorrect')
        }

    };

    render() {
        return (
            <Container style={styles.container}>
                <Header style={{ alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontSize: 20 }}>Login</Text>
                </Header>
                <MainContent>

                    <DarkInput
                        placeholder="Email"
                        value={this.state.email}
                        placeholderTextColor='white'
                        onChangeText={val => {
                            this.onEmailChange(val);
                        }}
                    />

                    {/* <View> */}
                        <DarkInput
                            placeholder="Password"
                            focus={() => this.setState({showPwdMeter: true})}
                            value={this.state.password}
                            placeholderTextColor='white'
                            onChangeText={val => {
                                this.onPasswordChange(val);
                            }}
                        />
                        {/* <View style={{width: '60%'}}> */}
                        {
                            this.state.showPwdMeter === true && (
                                <BarPasswordStrengthDisplay
                        
                                password={this.state.password}
                            />
                            )
                        }
                        {/* </View> */}
                        
                       

                    {/* </View> */}



                    <TouchableOpacity>
                        <Button rounded onPress={this.onPress} style={{ margin: 10, textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <Text>Login</Text>
                        </Button>
                    </TouchableOpacity>
                </MainContent>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {},
    content: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center'
    },
    form: {
        width: '100%'
    },
    item: {}
});