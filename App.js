import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from "react-navigation-stack";

import Login from './src/screens/Login/Login';
import Dashboard from './src/screens/Dashboard/Dashboard';



export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const AppNavigator = createStackNavigator({
  
  Home: {
    screen: Login,
    navigationOptions: {
      header: null,
  },
  },
  Dashboard: {
    screen: Dashboard,
    navigationOptions: {
      header: null,
  },
  },
  // EmployeeList: {
  //   screen: EmployeeList,
  //   navigationOptions: {
  //     header: null,
  // },
  // }
});

const AppContainer = createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});